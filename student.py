#! /usr/bin/env python

def main():
    msg = "Hallo, World"
    print(msg)
    with open("data.txt", "r", encoding='utf-8') as file_:
        for line in file_:
            fio, login = line.split(';')
            print(f"fio:{fio}, login:{login}")

if __name__ == '__main__':
    main()
